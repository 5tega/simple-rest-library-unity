using System;
using MarbleIT.SimpleRESTLib;

public class MockAPI : RESTClient
{
    public static RESTEndpoint users;
    public static RESTEndpoint login;
    public static RESTClientConfigurator configuration;

    private void Awake()
    {
        configuration = new RESTClientConfigurator(this);
        users = new RESTEndpoint("users/", this);
    }
}