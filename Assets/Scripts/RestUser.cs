﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class RestUser : MonoBehaviour
{
    [Serializable]
    public class User
    {
        public int id;
        public string createdAt;
        public string name;
        public string avatar;

        public override string ToString()
        {
            return $"{id}, {createdAt}, {name}, {avatar}";
        }
    }

    private void Start()
    {
        User user = new User();
        user.name = "bla bla";
        user.avatar = "ksksksks.png";

        MockAPI.users.Get<User>("1")
            .Then(response =>
            {
                Debug.Log(response.body);
            });

        MockAPI.users.GetList<User>()
            .Then(response =>
            {
                foreach (var userDto in response.body)
                {
                    Debug.Log(userDto);
                }
            });

        MockAPI.users.Post(user)
            .Then(response =>
            {
                Debug.Log(response.statusCode);
                Debug.Log(response.body);
            });

        MockAPI.users.Put(user, "51")
            .Then(response =>
            {
                Debug.Log(response.body);
            })
            .Catch(error =>
            {
                Debug.Log("An error happened at PUT");
            });

        MockAPI.users.Delete<User>("51")
            .Then(response =>
            {
                Debug.Log(user);
            })
            .Catch(error =>
            {
                Debug.Log("An error happened at DELETE");
            });


        MockAPI.login.Post<AuthDTO>(new AuthDTO())
            .Then(response =>
            {
                MockAPI.configuration.AuthorizationToken = response.body.token;
            });
    }

    private class AuthDTO
    {
        public string token;
    }
}