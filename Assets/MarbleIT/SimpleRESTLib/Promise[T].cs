﻿using System;
using System.Collections.Generic;

namespace MarbleIT.SimpleRESTLib
{
    public class Promise<T>
    {
        private PromiseState _state;

        private Queue<Promise<T>> _nextPromises;
        private Action<T> _thenCallback;
        private Action<Exception> _catchCallback;
        private T _payload;
        private Exception _error;
        
        public Promise()
        {
            _state = PromiseState.Pending;
            _nextPromises = new Queue<Promise<T>>();
        }

        public Promise<T> Then(Action<T> callback)
        {
            Promise<T> promise = new Promise<T> { _thenCallback = callback };
            
            switch (_state)
            {
                case PromiseState.Fulfilled:
                    promise.Resolve(_payload);
                    break;
                case PromiseState.Failed:
                    promise.Reject(_error);
                    break;
                default:
                    _nextPromises.Enqueue(promise);
                    break;
            }
            
            return promise;
        }
        
        public Promise<T> Catch(Action<Exception> callback)
        {
            Promise<T> promise = new Promise<T> { _catchCallback = callback };
            
            switch (_state)
            {
                case PromiseState.Fulfilled:
                    promise.Resolve(_payload);
                    break;
                case PromiseState.Failed:
                    promise.Reject(_error);
                    break;
                default:
                    _nextPromises.Enqueue(promise);
                    break;
            }
            
            return promise;
        }

        public void Resolve(T payload)
        {
            if (_state != PromiseState.Pending)
            {
                return;
            }

            try
            {
                _thenCallback?.Invoke(payload);
                _state = PromiseState.Fulfilled;
                _payload = payload;
            }
            catch (Exception error)
            {
                Reject(error);
                return;
            }
            
            while (_nextPromises.Count > 0)
            {
                _nextPromises.Dequeue().Resolve(payload);
            }
        }

        public void Reject(Exception error)
        {
            if (_state != PromiseState.Pending)
            {
                return;
            }
            
            _state = PromiseState.Failed;
            _error = error;

            if (_catchCallback != null)
            {
                _catchCallback.Invoke(error);
                while (_nextPromises.Count > 0)
                {
                    _nextPromises.Dequeue().Resolve(default(T));
                }    
            }
            else
            {
                while (_nextPromises.Count > 0)
                {
                    _nextPromises.Dequeue().Reject(error);
                }
            }
            
            
        }
    }
}
