namespace MarbleIT.SimpleRESTLib
{
    public struct Response<T>
    {
        public T body;
        public long statusCode;

        public Response(T body, long statusCode)
        {
            this.body = body;
            this.statusCode = statusCode;
        }
    }
}