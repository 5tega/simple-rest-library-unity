namespace MarbleIT.SimpleRESTLib
{
    public class RESTClientConfigurator
    {
        private RESTClient _restClient;

        public string ServerURL
        {
            get
            {
                return _restClient.serverUrl;
            }

            set
            {
                _restClient.serverUrl = value;
            }
        }

        public string AuthorizationToken
        {
            get
            {
                return _restClient.authorizationToken;
            }

            set
            {
                _restClient.authorizationToken = value;
            }
        }
        
        public RESTClientConfigurator(RESTClient restClient)
        {
            _restClient = restClient;
        }
    }
}