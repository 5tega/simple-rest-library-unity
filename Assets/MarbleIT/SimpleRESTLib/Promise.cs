﻿using System;
using System.Collections.Generic;

namespace MarbleIT.SimpleRESTLib
{
    public class Promise
    {
        private PromiseState _state;

        private Queue<Promise> _nextPromises;
        private Action _thenCallback;
        private Action<Exception> _catchCallback;
        private Exception _error;
        
        public Promise()
        {
            _state = PromiseState.Pending;
            _nextPromises = new Queue<Promise>();
        }

        public Promise Then(Action callback)
        {
            Promise promise = new Promise { _thenCallback = callback };
            
            switch (_state)
            {
                case PromiseState.Fulfilled:
                    promise.Resolve();
                    break;
                case PromiseState.Failed:
                    promise.Reject(_error);
                    break;
                default:
                    _nextPromises.Enqueue(promise);
                    break;
            }
            
            return promise;
        }
        
        public Promise Catch(Action<Exception> callback)
        {
            Promise promise = new Promise { _catchCallback = callback };
            
            switch (_state)
            {
                case PromiseState.Fulfilled:
                    promise.Resolve();
                    break;
                case PromiseState.Failed:
                    promise.Reject(_error);
                    break;
                default:
                    _nextPromises.Enqueue(promise);
                    break;
            }
            
            return promise;
        }

        public void Resolve()
        {
            if (_state != PromiseState.Pending)
            {
                return;
            }

            try
            {
                _thenCallback?.Invoke();
                _state = PromiseState.Fulfilled;
            }
            catch (Exception error)
            {
                Reject(error);
                return;
            }
            
            while (_nextPromises.Count > 0)
            {
                _nextPromises.Dequeue().Resolve();
            }
        }

        public void Reject(Exception error)
        {
            if (_state != PromiseState.Pending)
            {
                return;
            }
            
            _state = PromiseState.Failed;
            _error = error;

            if (_catchCallback != null)
            {
                _catchCallback.Invoke(error);
                while (_nextPromises.Count > 0)
                {
                    _nextPromises.Dequeue().Resolve();
                }    
            }
            else
            {
                while (_nextPromises.Count > 0)
                {
                    _nextPromises.Dequeue().Reject(error);
                }
            }
            
            
        }
    }
}
