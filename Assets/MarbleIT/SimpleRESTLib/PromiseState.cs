﻿namespace MarbleIT.SimpleRESTLib
{
    public enum PromiseState
    {
        Pending,
        Fulfilled,
        Failed
    }
}