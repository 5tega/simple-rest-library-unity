var searchData=
[
  ['reject_68',['Reject',['../class_marble_i_t_1_1_simple_r_e_s_t_lib_1_1_promise.html#ad56031b9b56c5f88c4e8149463190bb9',1,'MarbleIT.SimpleRESTLib.Promise.Reject(Exception error)'],['../class_marble_i_t_1_1_simple_r_e_s_t_lib_1_1_promise.html#ad56031b9b56c5f88c4e8149463190bb9',1,'MarbleIT.SimpleRESTLib.Promise.Reject(Exception error)']]],
  ['resolve_69',['Resolve',['../class_marble_i_t_1_1_simple_r_e_s_t_lib_1_1_promise.html#a8b14833305785674b0192a8f959f4170',1,'MarbleIT.SimpleRESTLib.Promise.Resolve()'],['../class_marble_i_t_1_1_simple_r_e_s_t_lib_1_1_promise.html#a5f328b44adfaa0d447f1d9bbd4263919',1,'MarbleIT.SimpleRESTLib.Promise.Resolve(T payload)']]],
  ['response_70',['Response',['../struct_marble_i_t_1_1_simple_r_e_s_t_lib_1_1_response.html#a10f7accacd55936dd88a55c86980ef51',1,'MarbleIT::SimpleRESTLib::Response']]],
  ['restclientconfigurator_71',['RESTClientConfigurator',['../class_marble_i_t_1_1_simple_r_e_s_t_lib_1_1_r_e_s_t_client_configurator.html#a091757a08f4131f46f9fe0de347abc20',1,'MarbleIT::SimpleRESTLib::RESTClientConfigurator']]],
  ['restendpoint_72',['RESTEndpoint',['../class_marble_i_t_1_1_simple_r_e_s_t_lib_1_1_r_e_s_t_endpoint.html#af3074eee188b5ec9a3a4afa7c54b7fd9',1,'MarbleIT::SimpleRESTLib::RESTEndpoint']]]
];
